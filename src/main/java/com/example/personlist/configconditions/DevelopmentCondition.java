package com.example.personlist.configconditions;

import com.google.common.collect.Lists;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * Проверка наличия профиля "dev" среди активных профилей spring
 */
public class DevelopmentCondition implements Condition {
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        return Lists.<String>newArrayList(context.getEnvironment().getActiveProfiles()).contains("dev");
    }
}
