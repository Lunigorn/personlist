package com.example.personlist.web.api.person;

import com.example.personlist.domain.Person;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Igor on 07.05.2015.
 */
public interface PersonDao {
    /**
     * JSON API для выборки Person по ФИО, названию машины и названию города.
     * Все параметры не обязательные, в случае их отсутсвия будет произведена выборка без условий.
     * Примеры запросов:
     * /api/persons/?car=ВАЗ
     * /api/persons?car=ВАЗ&city=Уфа
     * /api/persons/?firstName=Елена&city=Сарапул
     * @param firstName Имя
     * @param lastName Фамилия
     * @param middleName Отчество
     * @param car Название машины
     * @param city Название города
     * @return массив найденных Person
     */
    List<Person> findByFullNameAndCityAndCar(String firstName, String lastName, String middleName, String car, String city);
}
