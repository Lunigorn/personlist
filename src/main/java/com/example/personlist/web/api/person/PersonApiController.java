package com.example.personlist.web.api.person;

import com.example.personlist.domain.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PersonApiController {
    @Autowired
    PersonDao dao;

    /**
     * JSON API для выборки Person по ФИО, названию машины и названию города.
     * Все параметры не обязательные, в случае их отсутсвия будет произведена выборка без условий.
     * Примеры запросов:
     * /api/persons/?car=ВАЗ
     * /api/persons?car=ВАЗ&city=Уфа
     * /api/persons/?firstName=Елена&city=Сарапул
     * @param firstName Имя
     * @param lastName Фамилия
     * @param middleName Отчество
     * @param car Название машины
     * @param city Название города
     * @return массив найденных Person
     */
    @RequestMapping(value = {"/api/persons", "/api/persons/"}, method = RequestMethod.GET)
    public ResponseEntity find(@RequestParam(value = "firstName", required = false) String firstName,
                               @RequestParam(value = "lastName", required = false) String lastName,
                               @RequestParam(value = "middleName", required = false) String middleName,
                               @RequestParam(value = "car", required = false) String car,
                               @RequestParam(value = "city", required = false) String city) {
        List<Person> persons = dao.findByFullNameAndCityAndCar(firstName, lastName, middleName, car, city);
        return new ResponseEntity(persons, HttpStatus.OK);
    }
}
