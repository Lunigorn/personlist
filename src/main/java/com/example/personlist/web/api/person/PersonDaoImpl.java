package com.example.personlist.web.api.person;

import com.example.personlist.domain.Person;
import com.example.personlist.domain.QCar;
import com.example.personlist.domain.QCity;
import com.example.personlist.domain.QPerson;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.hibernate.HibernateQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public class PersonDaoImpl implements PersonDao {
    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Поиск Person по ФИО, названию машины и названию города.
     * Все параметры не обязательные, в случае их отсутсвия будет произведена выборка без условий.
     * @param firstName Имя
     * @param lastName Фамилия
     * @param middleName Отчество
     * @param car Название машины
     * @param city Название города
     * @return массив найденных Person с связанными lazy сущностями
     */
    @Override
    @Transactional(value = Transactional.TxType.REQUIRED)
    public List<Person> findByFullNameAndCityAndCar(String firstName, String lastName, String middleName, String car, String city){
        JPQLQuery query = new HibernateQuery(sessionFactory.getCurrentSession());
        QPerson p = QPerson.person;
        QCar cr = QCar.car;
        QCity cy = QCity.city;
        // выборка Person и подтягивание связанных lazy сущностей
        query.from(p).leftJoin(p.city, cy).leftJoin(p.cars, cr).fetch().distinct().orderBy(p.id.asc());
        // выборка по опциональным парметрам
        Optional.ofNullable(firstName).ifPresent( fn -> query.where(p.firstName.contains(fn)));
        Optional.ofNullable(lastName).ifPresent(ln -> query.where(p.lastName.contains(ln)));
        Optional.ofNullable(middleName).ifPresent(mn -> query.where(p.middleName.contains(mn)));
        Optional.ofNullable(city).ifPresent(ct -> query.where(p.city.name.contains(ct)));
        Optional.ofNullable(car).ifPresent(ca -> query.where(p.cars.any().name.contains(ca)));
        return query.list(p);
    }
}
