package com.example.personlist.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "Car")
public class Car {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    protected Long id;
    @Column(nullable = false)
    @JsonProperty
    protected String name;
    @ManyToMany(mappedBy = "cars")
    protected Set<Person> persons;
}
