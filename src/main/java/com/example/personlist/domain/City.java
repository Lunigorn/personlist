package com.example.personlist.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "City")
public class City {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    protected Long id;
    @Column(nullable = false)
    @JsonProperty
    protected String name;
    @OneToMany(mappedBy = "city")
    protected Set<Person> persons;
}
