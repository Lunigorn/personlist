package com.example.personlist.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Table(name = "Person")
public class Person {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @JsonProperty
    protected Long id;
    @Column(nullable = false)
    @JsonProperty
    protected String firstName;
    @Column(nullable = false)
    @JsonProperty
    protected String lastName;
    @JsonProperty
    protected String middleName;
    @ManyToOne
    @JoinColumn(name = "cityId")
    @JsonProperty
    protected City city;
    @ManyToMany
    @JoinTable(name = "PersonToCar",
            joinColumns = { @JoinColumn(name = "personId") },
            inverseJoinColumns = { @JoinColumn(name = "carId")},
            uniqueConstraints = {
                    @UniqueConstraint(columnNames = {
                            "personId",
                            "carId"
                    })})
    @JsonProperty
    protected Set<Car> cars;
}
