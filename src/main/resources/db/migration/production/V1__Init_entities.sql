CREATE SEQUENCE cityIdSeq START 1;
CREATE TABLE City(
        id INTEGER PRIMARY KEY DEFAULT nextval('cityIdSeq'),
        name VARCHAR(100) NOT NULL);

CREATE SEQUENCE personIdSeq START 1;
CREATE TABLE Person(
        id INTEGER PRIMARY KEY DEFAULT nextval('personIdSeq'),
        firstName VARCHAR(100) NOT NULL,
        lastName VARCHAR(100) NOT NULL,
        middleName VARCHAR(100),
        cityId INTEGER NOT NULL REFERENCES City(id) ON DELETE RESTRICT);

CREATE SEQUENCE carIdSeq START 1;
CREATE TABLE Car(
        id INTEGER PRIMARY KEY DEFAULT nextval('carIdSeq'),
        name VARCHAR(100) NOT NULL);

CREATE TABLE PersonToCar(
        personId INTEGER NOT NULL REFERENCES Person(id) ON DELETE RESTRICT,
        carId INTEGER NOT NULL REFERENCES Car(id) ON DELETE RESTRICT,
        CONSTRAINT HyipToPaymentSystemPKey PRIMARY KEY (personId, carId));