<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="/resources/css/index.css"/>
    <title></title>
</head>
<body>
<span id="error" hidden>Необходимо заполнить хотя бы одно поле<br/></span>
<label>
    Фамилия
    <input class="field" id="lastName" type="text"/>
</label>
<label>
    Имя
    <input class="field" id="firstName" type="text"/>
</label>
<label>
    Отчество
    <input class="field" id="middleName" type="text"/>
</label>
<br/>
<label>
    Город
    <input class="field" id="city" type="text"/>
</label>
<label>
    Машина
    <input class="field" id="car" type="text"/>
</label>
<input id="search" type="button" value="искать"/>
<table id="persons">
    <tr>
        <td>Фамилия</td><td>Имя</td><td>Отчество</td><td>Город</td><td>Машины</td>
    </tr>

</table>
<script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="/resources/js/index.js"></script>
</body>
</html>