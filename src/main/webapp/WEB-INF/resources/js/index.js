$("#search").on( "click", function( event ) {
    var searchForm = {};
    searchForm.lastName = $("#lastName").val();
    searchForm.firstName = $("#firstName").val();
    searchForm.middleName = $("#middleName").val();
    searchForm.city = $("#city").val();
    searchForm.car = $("#car").val();
    if (searchForm.lastName == "" && searchForm.firstName=="" && searchForm.middleName=="" && searchForm.city==""&& searchForm.car==""){
        $("#error").show();
    }else{
        var data = {};
        for (var field in searchForm){
            if (searchForm[field]!= "") data[field] = searchForm[field];
        }
        $.ajax('api/persons', {
            data:data,
            dataType: 'json',
            success: function(data) {
                $('#persons').find('tr').not(':first').remove();
                var html = '';
                for(var i = 0; i < data.length; i++)
                    html += '<tr><td>' + data[i].lastName + '</td><td>' +
                    data[i].firstName  + '</td><td>' +
                    data[i].middleName  + '</td><td>' +
                    data[i].city.name  + '</td><td>' +
                    jQuery.map(data[i].cars, function(v){return v.name}).join(', ') + '</td></tr>';
                $('#persons').find('tr').first().after(html);
            }
        });
    }
});
$('.field').on("click", function(){
    $("#error").hide();
});